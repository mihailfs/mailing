import pytz

from django.db import models
from django.db.models.signals import post_save

from phonenumber_field.modelfields import PhoneNumberField

from probe.base_models import BaseModel

import notifier.tasks as tasks


class Client(BaseModel):
    """Модель клиента"""

    TIMEZONE_CHOICES = zip(
        pytz.all_timezones,
        pytz.all_timezones
    )

    phone_number = PhoneNumberField(
        verbose_name="Номер телефона",
        unique=True
    )

    phone_code = PhoneNumberField(
        verbose_name="Код оператора",
        editable=False,
        null=True,
        blank=True,
    )

    timezone = models.CharField(
        max_length=255,
        default='UTC',
        choices=TIMEZONE_CHOICES
    )

    tag = models.CharField(
        verbose_name="Тег",
        max_length=128
    )

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    def save(self, *args, **kwargs):
        self.phone_code = str(self.phone_number)[2:5]
        super(Client, self).save(*args, **kwargs)

    def __str__(self) -> str:
        return f'{self.phone_number} {self.timezone}'


class Mailing(BaseModel):
    """Модель рассылки"""

    start_at = models.DateTimeField(
        verbose_name="Дата старта рассылки",
    )

    end_at = models.DateTimeField(
        verbose_name="Дата окончания рассылки",
    )

    text = models.CharField(
        verbose_name="Сообщение",
        max_length=255
    )

    filter = models.JSONField(
        verbose_name="Фильтр"
    )

    was_sent = models.BooleanField(
        verbose_name="Рассылка была отправлена",
        default=False
    )

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"

    def run(self):
        """Запуск рассылки"""
        tasks.task_run_mailing.delay(self)

    def __str__(self) -> str:
        return f'"{self.text}" с {self.start_at} по {self.end_at}'


class Message(BaseModel):
    """Модель сообщения"""

    class MessageStatus(models.TextChoices):
        CREATED = 'CREATED', 'Создано'
        QUEUED = 'QUEUED', 'В очереди на отправку'
        SENT_ERROR_API = 'SENT_ERROR_API', 'Ошибка отправления из-за API'
        SENT_ERROR_SERVICE = 'SENT_ERROR_SERVICE', 'Ошибка отправления у нас'
        SENT_SUCCESS = 'SENT_SUCCESS', 'Успешно отправлено'

    status = models.CharField(
        max_length=32,
        choices=MessageStatus.choices,
        default=MessageStatus.CREATED,
    )

    sent_at = models.DateTimeField(
        verbose_name="Отправлено",
        blank=True,
        null=True
    )

    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.CASCADE,
        verbose_name="Рассылка"
    )

    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        verbose_name="Клиент"
    )

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

    def __str__(self) -> str:
        return f'{self.mailing} по факту отправлено {self.sent_at} для {self.client}'


def after_message_saved(sender, instance: Message, *args, **kwargs):
    """Событие вызываемое после сохранения сообщения в БД"""
    msg = instance

    # CREATED -> QUEUED без условий
    # if msg.status == MessageStatus.CREATED:
    #     msg.status = MessageStatus.QUEUED
    #     msg.save()
    if msg.status == Message.MessageStatus.QUEUED:
        # отложенная отправка таска
        tasks.task_send_message.apply_async(args=[msg.id], eta=msg.mailing.start_at)


def after_mailing_saved(sender, instance: Mailing, *args, **kwargs):
    """Событие вызываемое после сохранения сообщения в БД"""
    if not instance.was_sent:
        tasks.task_run_mailing.delay(instance.id)


post_save.connect(after_message_saved, sender=Message)
post_save.connect(after_mailing_saved, sender=Mailing)
