from django.contrib import admin

from notifier.models import (
    Mailing,
    Client,
    Message
)


@admin.register(Message)
class OrderAdmin(admin.ModelAdmin):
    list_display = ("client", "status", "mailing",  "created_at", "updated_at", "sent_at")
    list_filter = ("status", "sent_at", "created_at", "updated_at")
    readonly_fields = ("client", "mailing",  "created_at", "updated_at", "sent_at")


@admin.register(Mailing)
class OrderAdmin(admin.ModelAdmin):
    list_display = ("text", "filter", "start_at", "end_at",)
    list_filter = ("start_at", "end_at", "created_at", "updated_at")
    readonly_fields = ("created_at", "updated_at")


@admin.register(Client)
class OrderAdmin(admin.ModelAdmin):
    list_display = ("phone_number", "timezone", "phone_code", "tag",  "created_at", "updated_at")
    list_filter = ("timezone",  "created_at", "updated_at", "phone_code")
    readonly_fields = ("phone_code",  "created_at", "updated_at")
