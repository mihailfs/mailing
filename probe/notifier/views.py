from django.db.models import Count


from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework.response import Response
from rest_framework.exceptions import NotFound

from notifier.models import Client, Mailing, Message
from notifier.serializers import (ClientSerializer,
                                  MailingSerializer,
                                  MessageSerializer,
                                  MailingStatSerializer
                                  )


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['timezone', 'tag']


class MailingViewSet(ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class MessageViewSet(ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MailingStatViewSet(ViewSet):
    """
    Получить статистику по статусам отправки сообщений в конкретной рассылке

    Пример:
        [
        {
            "status": "QUEUED",
            "total": 1
        },
        {
            "status": "SENT_ERROR_API",
            "total": 1
        }
        ]
    """
    def retrieve(self, request, pk=None):
        mailing = Mailing.objects.filter(pk=pk).first()
        if mailing is None:
            raise NotFound(detail="Указанная рассылка не найдена", code=404)
        # статистика по статусам рассылки сообщений
        query = Message.objects.values("status").filter(mailing=mailing).annotate(total=Count("id"))
        serializer = MailingStatSerializer(query, many=True)
        return Response(serializer.data)
