from django.utils.timezone import now  # , make_aware

from celery import shared_task
from celery.utils.log import get_task_logger

import notifier.models as models

from notifier.remote_api import send_msg

logger = get_task_logger(__name__)


@shared_task()
def task_send_message(msg_id: int):
    logger.info(f'send task message id: {msg_id}')
    # проверки
    if not isinstance(msg_id, int):
        logger.error("task send message got not integer in arg, skip it")
        return
    msg = models.Message.objects.filter(pk=msg_id).first()
    if msg is None:
        return
    if msg.status in (models.Message.MessageStatus.CREATED, models.Message.MessageStatus.SENT_SUCCESS):
        return
    # отправка
    phone = str(msg.client.phone_number)[1:]
    text = str(msg.mailing.text)
    try:
        response = send_msg(msg.id, phone, text)
    except Exception as e:
        msg.status = models.Message.MessageStatus.SENT_ERROR_SERVICE
        msg.save()
        logger.error(f'msg {msg.id} wasnt sent {str(e)}')
        return
    logger.debug(f'SEND MESSAGE: to {phone} "{text}"')
    print(f'RESPONSE CODE: {response.status_code}')
    if response.status_code == 200:
        msg.status = models.Message.MessageStatus.SENT_SUCCESS
    else:
        msg.status = models.Message.MessageStatus.SENT_ERROR_API
    msg.sent_at = now()
    msg.save()


@shared_task()
def task_run_mailing(mailing_id: int):
    """Запуск рассылки"""
    # получаем фильтры

    mailing = models.Mailing.objects.filter(id=mailing_id).first()
    if mailing is None:
        return
    logger.info('task_run_mailing started')
    filter_code = mailing.filter.get("code", None)
    filter_tag = mailing.filter.get("tag", None)
    logger.info(f'FILTERS: {filter_tag} + {filter_code}')
    # собираем подходящих клиентов
    filt_list = {}
    if filter_tag is not None:
        filt_list["tag"] = filter_tag
    if filter_code is not None:
        filt_list["phone_code"] = filter_code
    q = models.Client.objects.filter(**filt_list)
    logger.info(f'подходящих по фильтру {q.count()}')
    msgs = []
    # создаем для каждого пользователя из фильтра свое сообщение
    for user in q:
        msgs.append(models.Message(status=models.Message.MessageStatus.QUEUED,
                                   mailing=mailing,
                                   client=user
                                   )
                    )
    models.Message.objects.bulk_create(msgs)
    mailing.was_sent = True
    mailing.save()
