import requests


def send_msg(msg_id: int, phone: str, text: str):
    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTA2MjA3MjMsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Ik1pa2VDYWtlcyJ9.HyTSXZhZ9zHqf6wUJ6-U9u6oA9iSAZAtupnTnnwvyys"
    url = "https://probe.fbrq.cloud/v1/send/" + str(msg_id)
    headers = {
        "Authorization": f'Bearer {token}',
        'Content-type': 'application/json', 'Accept': '*/*'
    }
    body = {
        "id": int(msg_id),
        "phone": int(phone),
        "text": text
    }
    return requests.post(
        url=url,
        json=body,
        headers=headers
    )