from rest_framework.serializers import (
                                        ModelSerializer,
                                        Serializer,
                                        CharField,
                                        IntegerField
                                        )

from notifier.models import Client, Mailing, Message


class ClientSerializer(ModelSerializer):

    class Meta:
        model = Client
        fields = '__all__'


class MailingSerializer(ModelSerializer):

    class Meta:
        model = Mailing
        fields = '__all__'


class MessageSerializer(ModelSerializer):

    class Meta:
        model = Message
        fields = '__all__'


class MailingStatSerializer(Serializer):
    status = CharField()
    total = IntegerField()

    class Meta:
        fields = ['status', 'total']
